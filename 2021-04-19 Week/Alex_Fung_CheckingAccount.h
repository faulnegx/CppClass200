/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-04-27
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef CHECKINGACCOUNT_H
#define CHECKINGACCOUNT_H

#include "Alex_Fung_Account.h"

class CheckingAccount : public Account {
  public:
    CheckingAccount(double = 0, double = 1);
    bool debit(double);
    bool credit(double);
    // Comment 8: getTransactionFee() needed to get transactionFee for assignment and new object creation.
    double getTransactionFee() const;
    friend std::ostream& operator<<(std::ostream&, const CheckingAccount&);
    bool operator+=(double);
    bool operator-=(double);
    CheckingAccount operator+(double) const;
    friend CheckingAccount operator+(double, const CheckingAccount&);
    CheckingAccount operator-(double) const;
    void operator=(const CheckingAccount&);
  private:
    double transactionFee;
};


#endif
