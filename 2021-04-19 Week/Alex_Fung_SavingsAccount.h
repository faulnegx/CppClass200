/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-04-27
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef SAVINGSACCOUNT_H
#define SAVINGSACCOUNT_H

#include "Alex_Fung_Account.h"

class SavingsAccount : public Account {
  public:
    SavingsAccount(double = 0, double = 0.01);
    double calculateInterest() const;
    // Comment 7: assignment operator and creating new instance requires 
    // access to interestRate, so getInterestRate is added.
    double getInterestRate() const;
    friend std::ostream& operator<<(std::ostream&, const SavingsAccount&);
    bool operator+=(double);
    bool operator-=(double);
    SavingsAccount operator+(double) const;
    friend SavingsAccount operator+(double, const SavingsAccount&);
    SavingsAccount operator-(double) const;
    void operator=(const SavingsAccount&);
  private:
    double interestRate;
};

#endif
