/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-04-27
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <iostream>

class Account {
  public:
    Account(double = 0);
    double getBalance() const;
    bool debit(double);
    bool credit(double);
    friend std::ostream& operator<<(std::ostream&, const Account&);
    bool operator+=(double);
    bool operator-=(double);
    Account operator+(double) const;
    friend Account operator+(double, const Account&);
    Account operator-(double) const;
    void operator=(const Account&);
  // Comment 1: balance is changed from private to protected to allow child 
  // classes to modify it directly
  protected:
    double balance;
};

#endif
