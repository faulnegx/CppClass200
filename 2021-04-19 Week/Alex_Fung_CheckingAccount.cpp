/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-04-27
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_CheckingAccount.h"

CheckingAccount::CheckingAccount(double initialBalance, double fee)
  : Account(initialBalance), transactionFee(fee) {
}

bool CheckingAccount::debit(double amount) {
  if (Account::debit(amount+transactionFee)) {
    std::cout << "$" << transactionFee << " transaction fee charged." << std::endl;
    return true;
  }
  return false;
}

bool CheckingAccount::credit(double amount) {
  if (Account::credit(amount-transactionFee)) {
    std::cout << "$" << transactionFee << " transaction fee charged." << std::endl;
    return true;
  }
  return false;
}

double CheckingAccount::getTransactionFee() const {
  return transactionFee;
}

std::ostream& operator<<(std::ostream& output, const CheckingAccount& c) {
  output << c.getBalance();
  return output;
}

bool CheckingAccount::operator+=(double amount) {
  return credit(amount);
}

bool CheckingAccount::operator-=(double amount) {
  return debit(amount);
}

CheckingAccount CheckingAccount::operator+(double amount) const {
  CheckingAccount newAccount{getBalance(), getTransactionFee()};
  newAccount.credit(amount);
  return newAccount;
}

CheckingAccount operator+(double amount, const CheckingAccount& a) {
  return a.operator+(amount);
}

CheckingAccount CheckingAccount::operator-(double amount) const {
  CheckingAccount newAccount{getBalance(), getTransactionFee()};
  newAccount.debit(amount);
  return newAccount;
}

// Comment 9: Assignment operator copies the data from other object to this object.
void CheckingAccount::operator=(const CheckingAccount& other) {
  balance = other.getBalance();
  transactionFee = other.getTransactionFee();
}
