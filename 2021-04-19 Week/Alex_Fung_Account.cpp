/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-04-27
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_Account.h"

Account::Account(double initialBalance): balance(initialBalance) {
  if (initialBalance < 0) {
    balance = 0;
    std::cout << "Initial balance must be greater than or equal to 0.0. ";
    std::cout << "Initial balance set to 0.0. " << std::endl;
  }
}

double Account::getBalance() const {
  return balance;
}

bool Account::debit(double amount) {
  if (balance < amount) {
    std::cout << "Debit amount exceeded account balance" << std::endl;
    return false;
  }
  balance -= amount;
  return true;
}

bool Account::credit(double amount) {
  if (amount < 0) {
    std::cout << "Cannot add negative credit to balance." << std::endl;
    return false;
  }
  balance += amount;
  return true;
}

std::ostream& operator<<(std::ostream& output, const Account& a) {
  // Comment 2: this output operator overload makes it easier to print account
  // balance.
  output <<  a.getBalance();
  return output;
}

bool Account::operator+=(double amount) {
  // Comment 3: += and -= use the credit and debit function internal to the class
  return credit(amount);
}

bool Account::operator-=(double amount) {
  return debit(amount);
}

Account Account::operator+(double amount) const {
  // Comment 4: Binary operator +/- requires the creation of a brand new object.
  Account newAccount{getBalance()};
  newAccount.credit(amount);
  return newAccount;
}

Account operator+(double amount, const Account& a) {
  // Comment 5: friend plus operator does the same thing as the reverse order.
  return a.operator+(amount);
}

Account Account::operator-(double amount) const {
  Account newAccount{getBalance()};
  newAccount.debit(amount);
  return newAccount;
}

void Account::operator=(const Account& other) {
  // Comment 6: Assignment operator copies data form other object to this object.
  balance = other.getBalance();
}
