#include <iostream>
#include <iomanip>
#include "Alex_Fung_Account.h"
#include "Alex_Fung_SavingsAccount.h"
#include "Alex_Fung_CheckingAccount.h"
using namespace std;

int main()
{
   Account account1( 711.0 ); 
   SavingsAccount account2( 247.0, 0.1 );
   CheckingAccount account3( 365.0, 5.0 );

   cout << fixed << setprecision( 2 );

   // Comment 10: Below are utilizes all of the new overloaded operators.
   cout << "account1 balance: $" << account1 << endl;
   cout << "account2 balance: $" << account2 << endl;
   cout << "account3 balance: $" << account3 << endl;
   cout << endl;

   account1+=51.0;
   account2+=77.0;
   account3+=25.0;
   
   cout << "After adding $51, account1 balance: $" << account1 << endl;
   cout << "After adding $77, account2 balance: $" << account2 << endl;
   cout << "After adding $25, account3 balance: $" << account3 << endl;
   cout << endl;

   account1-=51.0;
   account2-=77.0;
   account3-=25.0;
   
   cout << "After subtracting $51, account1 balance: $" << account1 << endl;
   cout << "After subtracting $77, account2 balance: $" << account2 << endl;
   cout << "After subtracting $25, account3 balance: $" << account3 << endl;
   cout << endl;

   Account testAccount1{100};
   cout << "New Account testAccount1 balance: $" << testAccount1 << endl;
   testAccount1 = testAccount1 + 4.0;
   cout << "After adding $4 to Account, balance: $" << testAccount1 << endl;
   testAccount1 = 3.0 + testAccount1;
   cout << "After adding Account to $3, balance: $" << testAccount1 << endl;
   testAccount1 = testAccount1 - 2.0;
   cout << "After subtracting $2, balance: $" << testAccount1 << endl;

   testAccount1 = account1;
   cout << "After assigning account1 to testAccount1, Balance: $";
   cout << testAccount1 << endl;
   cout << endl;

   SavingsAccount testAccount2{200, 0.25};
   cout << "New Savings Account testAccount2 balance $" << testAccount2 << endl;
   testAccount2 = testAccount2 + 6.0;
   cout << "After adding $6 to Savings Account, balance: $" << testAccount2 << endl;
   testAccount2 = 5.0 + testAccount2;
   cout << "After adding Saving Account to $5, balance: $" << testAccount2 << endl;
   testAccount2 = testAccount2 - 4;
   cout << "After subtracting $4, balance: $" << testAccount2 << endl;

   testAccount2 = account2;
   cout << "After assigning account2 to testAccount2, balance: $" << testAccount2 << endl;
   cout << endl;

   CheckingAccount testAccount3{300, 5};
   cout << "New Checking Account testAccount3 balance $" << testAccount3 << endl;
   testAccount3 = testAccount3 + 5.0;
   cout << "After adding $5 to Checking Account, balance: $" << testAccount3 << endl;
   testAccount3 = 6.0 + testAccount3;
   cout << "After adding Checking Account to $6, balance: $" << testAccount3 << endl;
   testAccount3 = testAccount3 - 125;
   cout << "After subtracting $125, balance: $" << testAccount3 << endl;

   testAccount3 = account3;
   cout << "Afer assigning account3 to testAccount3, balance $" << testAccount3 << endl;

   // system("PAUSE");
   return 0;
   
}
