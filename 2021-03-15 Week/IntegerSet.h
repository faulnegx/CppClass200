/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-04-06
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef INTEGERSET_H
#define INTEGERSET_H

#include <iostream> 
#include <iomanip>
#include <vector>

class IntegerSet {
  public:
    IntegerSet();
    IntegerSet(int [], int);
    void insertElement(int);
    void deleteElement(int);
    void printSet() const;
    void inputSet();
    IntegerSet unionOfSets(const IntegerSet&) const;
    IntegerSet intersectionOfSets(const IntegerSet&) const;
    bool isEqualTo(const IntegerSet&) const;
    // Comment 1: Added isInSet Helper public function to check if an int is in a set.
    bool isInSet(int) const;
  private:
    std::vector<bool> set;
    // Comment 2: Added isValidInt Helper private function to check if int is valid.
    bool isValidInt(int) const;
    // Comment 3: Added a static constant to avoid repeating hard coded value of 101
    static const int kSetSize;
};

#endif
