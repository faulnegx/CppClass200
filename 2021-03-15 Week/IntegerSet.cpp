/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-04-06
Programming Environment Used: Linux, gcc 8.3
*/

#include "IntegerSet.h"
// Comment 4: For 0-100, there will be 101 elements in our private vector. 
const int IntegerSet::kSetSize = 101;

IntegerSet::IntegerSet(): set(kSetSize, false) {
}

IntegerSet::IntegerSet(int array[], int size): set(kSetSize, false) {
  // Comment 5: When provided with an array, insert each element into internal set.
  for (int i = 0; i < size; ++i) {
    insertElement(array[i]);
  }
}

void IntegerSet::insertElement(int e) {
  if (isValidInt(e)) {
    set[static_cast<size_t>(e)] = true;
  } else {
    std::cout << "Invalid insert attempted!" << std::endl;
  }
}

void IntegerSet::deleteElement(int e) {
  if (isValidInt(e)) {
    set[static_cast<size_t>(e)] = false;
  } else {
    std::cout << "Invalid delete attempted!" << std::endl;
  }
}

void IntegerSet::printSet() const {
  std::cout << "{";
  bool isEmpty = true;
  for (int i = 0; i < kSetSize; ++i) {
    if (set[static_cast<size_t>(i)]) {
      // Comment 6: If any element of set is true, then the set is not empty.
      isEmpty = false;
      std::cout << std::setw(4) << i; 
    }
  }
  if (isEmpty) {
    std::cout << std::setw(4) << "---";
  }
  std::cout << std::setw(4) << "}" << std::endl;
}

void IntegerSet::inputSet() {
  std::string buff;
  int inputVal = 0;
  while (inputVal != -1) {
    std::cout << "Enter an element (-1 to end): ";
    std::cin >> buff;
    // Comment 7: If buff cannot be converted to an integer, this is a bug since atoi 
    // will return 0, and pretend everything is okay. 
    inputVal = atoi(buff.c_str());
    if (inputVal == -1) {
      std::cout << "Entry complete" << std::endl;
    } else {
      // Comment 8: Check inputVal is a valid int prior to inserting element since there
      // is no check insde insertElement function.
      if (isValidInt(inputVal)) {
        insertElement(inputVal);
      } else {
        std::cout << "Invalid Element" << std::endl;
      }
    }
  }
}

IntegerSet IntegerSet::unionOfSets(const IntegerSet& otherSet) const {
  IntegerSet output;
  for (int i = 0; i < kSetSize; ++i) {
    // Comment 9: Using OR here since if either set's value is true, output set shall 
    // contain that value.
    if (set[static_cast<size_t>(i)] || otherSet.isInSet(i)) {
      output.insertElement(i);
    }
  }
  return output;
}

IntegerSet IntegerSet::intersectionOfSets(const IntegerSet& otherSet) const {
  IntegerSet output;
  for (int i = 0; i < kSetSize; ++i) {
    // Comment 10: Using AND here since if both set's values are true, then output 
    // set shall contain that value.
    if (set[static_cast<size_t>(i)] && otherSet.isInSet(i)) {
      output.insertElement(i);
    }
  }
  return output;
}

bool IntegerSet::isEqualTo(const IntegerSet& otherSet) const {
  // Comment 11: Compare all items in set to determine if the sets are equal.
  for (int i = 0; i < kSetSize; ++i) {
    if (set[static_cast<size_t>(i)] != otherSet.isInSet(i)) {
      return false;
    }
  }
  return true;
}

bool IntegerSet::isInSet(int val) const {
  if (isValidInt(val)) {
    return set[static_cast<size_t>(val)];
  } else {
    std::cout << "Invalid value to check" << std::endl;
    return false;
  }
}

bool IntegerSet::isValidInt(int num) const {
  // Comment 12: Only return true if num is within the range of our set size.
  if (num >= 0 && num < kSetSize) {
    return true;
  }
  return false;
}
