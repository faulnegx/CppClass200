/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-04-06
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef POLYNOMIAL_H
#define POLYNOMIAL_H

#include <cstdlib> // atoi
#include <iostream>
#include <limits> // std::numeric_limits<T>::max()s
#include <unordered_set>
#include <vector>

// Comment 1: Each term contains coefficient and exponent
struct Term {
  Term();
  Term(int, int);
  int coefficient;
  int exponent;
};

class Polynomial {
  public:
    Polynomial();
    void enterTerms();
    std::vector<Term> getTerms() const;
    void setTerm(const Term&);
    void printPolynomial() const;
    // Comment 2: Addition, subtraction, and negating returns a brand new polynomial
    Polynomial operator+(const Polynomial&) const;
    Polynomial operator-(const Polynomial&) const;
    Polynomial operator-() const;
    const Polynomial& operator=(const Polynomial&);
    void operator+=(const Polynomial&);
    void operator-=(const Polynomial&);
    int searchExponent(int exp) const;
    int searchCoefficient(int co) const;
    // Comment 3: drop0CoefficientTerms and sortTerms keeps terms vector always sorted and clean
    void drop0CoefficientTerms();
    void sortTerms();
  private:
    std::vector<Term> terms;
};

#endif
