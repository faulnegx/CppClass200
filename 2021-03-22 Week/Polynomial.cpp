/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-04-06
Programming Environment Used: Linux, gcc 8.3
*/

#include "Polynomial.h"

// Comment 4: Term constructors
Term::Term() :coefficient(0), exponent(0) {}
Term::Term(int co, int exp) :coefficient(co), exponent(exp) {}

Polynomial::Polynomial(): terms() {}

void Polynomial::enterTerms() {
  std::cout << "\nEnter number of polynomial terms: ";
  std::string buf;
  std::cin >> buf;
  // Comment 5: The following atoi calls will truncate floats to int, and it will 
  // also turn non-numerical values to 0 
  int count = atoi(buf.c_str());
  for (int i = 0; i < count; ++i) {
    std::cout << "\nEnter coefficient: ";
    std::cin >> buf;
    int coefficient = atoi(buf.c_str());
    std::cout << "Enter exponent: ";
    std::cin >> buf;
    int exponent = atoi(buf.c_str());
    setTerm(Term(coefficient, exponent));
  }
}

std::vector<Term> Polynomial::getTerms() const {
  return terms;
}

// Comment 6: set new Term into Polynomial. If it is already in Polynomial, replace the
// Term
void Polynomial::setTerm(const Term& newTerm) {
  int foundIndex = searchExponent(newTerm.exponent);
  if (foundIndex != std::numeric_limits<int>::max()) {
    terms[foundIndex] = newTerm;
  } else {
    terms.push_back(newTerm);
    sortTerms();
  }
  drop0CoefficientTerms();
}

void Polynomial::printPolynomial() const {
  if (terms.size() == 0) {
    std::cout << 0 << std::endl;
    return;
  }
  // Comment 7: Print Coefficient of x^0 first
  bool alreadyPrintedSomething = false;
  for (int i = 0; i < static_cast<int>(terms.size()); ++i) {
    if (terms[i].exponent == 0) {
      std::cout << terms[i].coefficient;
      alreadyPrintedSomething = true;
    }
  }
  // Comment 8: Print the rest of the terms starting from the highest exponent
  for (int i = (int) terms.size()-1; i >= 0; --i) {
    if (terms[i].exponent != 0) {
      if (alreadyPrintedSomething && terms[i].coefficient>=0) {
        std::cout << "+";
      }
      if (terms[i].coefficient == -1) {
        std::cout << "-";
      } else if (terms[i].coefficient != 1) {
        std::cout << terms[i].coefficient;
      } 
      if (terms[i].exponent == 1) {
        std::cout << "x";
      } else {
        std::cout << "x^" << terms[i].exponent;
        alreadyPrintedSomething = true;
      }
    }
  }
  std::cout << std::endl;
}

Polynomial Polynomial::operator+(const Polynomial& other) const {
  // Comment 9: Temporary get a copy of other's terms
  std::vector<Term> otherTerms = other.getTerms();
  // Comment 10: Create a set to jot down which exponent has been processed.
  std::unordered_set<int> processedExponents;
  // Comment 11: Copy construct a new polynomial. We will be changing this new polynomial
  // This way we do not make any changes to this or other polynomial. 
  Polynomial res(*this);
  for (int i = 0; i < static_cast<int>(terms.size()); ++i) {
    int foundIndex = other.searchExponent(terms[i].exponent);
    if (foundIndex != std::numeric_limits<int>::max()) {
      res.setTerm(
        Term(
          terms[i].coefficient + otherTerms[foundIndex].coefficient, 
          terms[i].exponent
        )
      );
    }
    // Comment 12: Note down that we have processed this exponent.
    processedExponents.insert(terms[i].exponent);
  }
  for (int i = 0; i < static_cast<int>(otherTerms.size()); ++i) {
    // Comment 13: If we have not already process this exponent, we will set the term 
    // in res.
    if (processedExponents.find(otherTerms[i].exponent) == processedExponents.end()) {
      res.setTerm(otherTerms[i]);
    }
  }
  return res;
}

Polynomial Polynomial::operator-(const Polynomial& other) const {
  return (*this) + -other;
}

Polynomial Polynomial::operator-() const {
  Polynomial res(*this);
  std::vector<Term> resTerms = getTerms();
  for (int i = 0; i < static_cast<int>(resTerms.size()); ++i) {
    res.setTerm(Term(resTerms[i].coefficient * -1, resTerms[i].exponent));
  }
  return res;
}

const Polynomial& Polynomial::operator=(const Polynomial& other) {
  // Comment 14: The whole vector is copied to this polynomial, not term by term. 
  this->terms = other.terms;
  return *this;
}

void Polynomial::operator+=(const Polynomial& other) {
  (*this) = (*this) + other;
}
void Polynomial::operator-=(const Polynomial& other) {
  (*this) = (*this) - other;
}

int Polynomial::searchExponent(int exp) const {
  for (int i = 0; i < static_cast<int>(terms.size()); ++i) {
    if (terms[i].exponent == exp) {
      return i;
    }
  }
  return std::numeric_limits<int>::max();
}

int Polynomial::searchCoefficient(int co) const {
  for (int i = 0; i < static_cast<int>(terms.size()); ++i) {
    if (terms[i].coefficient == co) {
      return i;
    }
  }
  return std::numeric_limits<int>::max();
}

// Comment 15: This function eliminate terms that have 0 as coefficient.
void Polynomial::drop0CoefficientTerms() {
  int foundIndex = searchCoefficient(0);
  while (foundIndex != std::numeric_limits<int>::max()) {
    terms.erase(terms.begin() + foundIndex);
    foundIndex = searchCoefficient(0);
  }
}

void Polynomial::sortTerms() {
  if (terms.size() <= 1) {
    return;
  }
  for (int i = 1; i < (int) terms.size(); ++i) {
    for (int swappingIndex = i; swappingIndex > 0; --swappingIndex) {
      if (terms[swappingIndex].exponent > terms[swappingIndex-1].exponent) {
        break;
      } else {
        std::swap(terms[swappingIndex], terms[swappingIndex-1]);
      }
    }
  }
}
