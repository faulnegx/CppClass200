/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-03-06
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef HEARTRATES_H
#define HEARTRATES_H

#include <cstdlib> // atoi
#include <iostream>
#include <string>

class HeartRates {
  public:
    HeartRates (std::string, std::string, int, int, int); 

    void setFirstName(std::string);
    void setLastName(std::string);
    std::string getFirstName() const;
    std::string getLastName() const;

    void setBirthYear(int);
    void setBirthMonth(int);
    void setBirthDay(int);
    int getBirthYear() const;
    int getBirthMonth() const;
    int getBirthDay() const;
    
    int getAge() const;
    std::string getTargetHeartRate(int) const;
    int getMaximumHeartRate(int) const;

  private:
    std::string firstName;
    std::string lastName;
    int birthYear;
    int birthMonth;
    int birthDay;
    // Comment 1: Constant that is not attached to HeartRates Instances
    static const int kBaseBeatsPerMinute{220};
};

#endif