/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-03-06
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_HeartRates.h"
#include <iostream>
#include <string>

void basicValidDate(int year, int month, int day) {
  if (year < 1900 || year > 2099) {
    // Comment 8: Choose to only allow recent years. There are some leap year rules we 
    // are avoiding. 2100, 2200, 2300. Also assuming gregorian calendar.
    throw "Invalid year, only 1901-2099 allowed";
  }
  if (month > 12 || month < 1) {
    throw "Invalid month";
  }
  if (day < 1) {
    throw "Invalid day";
  } else if ((month == 1 || month == 3 || month == 5 || month == 7 
    || month == 8 || month == 10 || month == 12) && (day > 31)) {
    throw "Invalid day";
  } else if ((month == 4 || month == 6 || month == 9 || month == 11) && (day > 30)) {
    throw "Invalid day";
  } else if ((month == 2) && (year % 4 == 0) && (day > 29)) {
    throw "Invalid day";
  } else if ((month == 2) && (day >28)) {
    throw "Invalid day";
  }
}

HeartRates promptsToCreateHeartRates() {
  // Comment 9: prompt user and create HeartRates instance.
  std::cout << "Please enter first and last name (separated by spaces): " << std::endl;
  std::string firstName;
  std::cin >> firstName;
  std::string lastName;
  std::cin >> lastName;
  std::cout << "Please enter month, day, and year of birth (separated by spaces): " << std::endl;
  std::string buff;
  std::cin >> buff;
  int month = atoi(buff.c_str());
  std::cin >> buff;
  int day = atoi(buff.c_str());
  std::cin >> buff;
  int year = atoi(buff.c_str());

  basicValidDate(year, month, day);
  HeartRates hr{firstName, lastName, year, month, day};
  return hr;
}

void printHeartRatesInfo (HeartRates& hr) {
  // Comment 10: print all member variables of a HeartRates instance
  std::cout << "First name: " << hr.getFirstName() << std::endl;
  std::cout << "Last Name: " << hr.getLastName() << std::endl;
  std::cout << "Date of Birth: " << hr.getBirthMonth() << "/" << hr.getBirthDay();
  std::cout << "/" << hr.getBirthYear() << std::endl;
}

int main() {

  HeartRates hr = promptsToCreateHeartRates();
  printHeartRatesInfo(hr);
  
  int age = hr.getAge();
  std::cout << "Age: " << age << std::endl;
  int maxHeartRate = hr.getMaximumHeartRate(age);
  std::cout << "Maximum Heart Rate: " << maxHeartRate << std::endl;
  std::string targetHeartRate = hr.getTargetHeartRate(maxHeartRate);
  std::cout << "Target Heart Rate: " << targetHeartRate << std::endl;

  return 0;
}
