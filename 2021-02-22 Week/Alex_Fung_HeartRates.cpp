/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-03-06
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_HeartRates.h"

HeartRates::HeartRates(std::string fname, std::string lname, int year, int month, int day) 
  : firstName{fname}, lastName{lname}, 
    birthYear{year}, birthMonth{month}, birthDay{day}
{
  // Comment 2: No data validation to see if year, month, day are reasonable inputs
}

void HeartRates::setFirstName(std::string fname) {
  firstName = fname;
}
void HeartRates::setLastName(std::string lname) {
  lastName = lname;
}
std::string HeartRates::getFirstName() const {
  return firstName;
}
std::string HeartRates::getLastName() const {
  return lastName;
}

void HeartRates::setBirthYear(int year) {
  birthYear = year;
}
void HeartRates::setBirthMonth(int month) {
  birthMonth = month;
}
void HeartRates::setBirthDay(int day) {
  birthDay = day;
}
int HeartRates::getBirthYear() const {
  return birthYear;
}
int HeartRates::getBirthMonth() const {
  return birthMonth;
}
int HeartRates::getBirthDay() const {
  return birthDay;
}

int HeartRates::getAge() const {
  std::cout << "Please enter today's month, day, and year" << std::endl;
  std::string buff;
  std::cin >> buff;
  int currentMonth = atoi(buff.c_str());
  std::cin >> buff;
  int currentDay = atoi(buff.c_str());
  std::cin >> buff;
  int currentYear = atoi(buff.c_str());
  // Comment 3: Ideally, I would have preferred to have some date validation outside of 
  // getAge(), then pass in the validated current date into getAge function. But the hw 
  // dictated that this prompt for current date to be inside getAge function. So there 
  // isn't any validation for user input of month, day, and year. 

  if (currentMonth > birthMonth) {
    // Comment 4: birthdate already passed this year
    return currentYear-birthYear;
  } else if (currentMonth < birthMonth) {
    // Comment 5: birthdate not yet passed this year
    return currentYear-birthYear-1;
  } else {
    // Comment 6: This left over case month == birthMonth, we will need to compare day.
    if (currentDay >= birthDay) {
      return currentYear-birthYear;
    } else {
      return currentYear-birthYear-1;
    }
  }
}

int HeartRates::getMaximumHeartRate(int age) const {
  return kBaseBeatsPerMinute - age;
}

std::string HeartRates::getTargetHeartRate(int maxHeartRate) const {
  // Comment 7: We avoided floating point number by using integer division, which
  // means we can have some rounding error. But this is sufficient precision for our
  // purposes.
  return std::to_string(maxHeartRate/2) + "-" + std::to_string(maxHeartRate*85/100);
}
