/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-04-20
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef SAVINGSACCOUNT_H
#define SAVINGSACCOUNT_H

#include "Alex_Fung_Account.h"

// Comment 5: SavingsAccount has public inheritance relationship with Account.
class SavingsAccount : public Account {
  public:
    SavingsAccount(double = 0, double = 0.01);
    double calculateInterest() const;
  private:
    double interestRate;
};

#endif
