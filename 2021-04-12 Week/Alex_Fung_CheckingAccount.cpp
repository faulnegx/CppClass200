/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-04-20
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_CheckingAccount.h"

CheckingAccount::CheckingAccount(double initialBalance, double fee)
  : Account(initialBalance), transactionFee(fee) {
}

// Comment 9: The child member function with the same function signature is overriding 
// parent's member function's behavior.
bool CheckingAccount::debit(double amount) {
  // Comment 10: Only debit if the balance has enough to pay for amount AND transaction
  // fee. 
  if (Account::debit(amount+transactionFee)) {
    std::cout << "$" << transactionFee << " transaction fee charged." << std::endl;
    return true;
  }
  return false;
}

bool CheckingAccount::credit(double amount) {
  if (Account::credit(amount-transactionFee)) {
    // Comment 11: Both debit and credit are transaction that fee are charged for. 
    std::cout << "$" << transactionFee << " transaction fee charged." << std::endl;
    return true;
  }
  return false;
}
