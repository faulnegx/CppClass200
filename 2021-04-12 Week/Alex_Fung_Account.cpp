/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-04-20
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_Account.h"

Account::Account(double initialBalance): balance(initialBalance) {
  // Comment 1: Validate initial balance to be >= 0. If not, set balance to 0.0 and 
  // display error message.
  if (initialBalance < 0) {
    balance = 0;
    std::cout << "Initial balance must be greater than or equal to 0.0. ";
    std::cout << "Initial balance set to 0.0. " << std::endl;
  }
}

double Account::getBalance() const {
  return balance;
}

bool Account::debit(double amount) {
  // Comment 2: If balance is less than amount debit, then 1. make no changes to balance, 
  // 2. display error message, and 3. return false to flag that debit failed.
  if (balance < amount) {
    std::cout << "Debit amount exceeded account balance" << std::endl;
    return false;
  }
  // Comment 3: If balance is sufficient, then update balance as expected and return
  // true to indicate debit is successful.
  balance -= amount;
  return true;
}

bool Account::credit(double amount) {
  // Comment 4: Validate that amount is >=0.
  if (amount < 0) {
    std::cout << "Cannot add negative credit to balance." << std::endl;
    return false;
  }
  balance += amount;
  return true;
}
