/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-04-20
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <iostream>

class Account {
  public:
    Account(double = 0);
    double getBalance() const;
    bool debit(double);
    bool credit(double);
  private:
    double balance;
};

#endif
