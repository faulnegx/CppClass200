/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-04-20
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef CHECKINGACCOUNT_H
#define CHECKINGACCOUNT_H

#include "Alex_Fung_Account.h"

// Comment 7: CheckingAccount has public inheritance relationship with Account.
class CheckingAccount : public Account {
  public:
    CheckingAccount(double = 0, double = 1);
    // Comment 8: CheckingAccount has its own debit and credit member function, with the
    // same name and parameter list(function signature) overriding the parent's debit 
    // and credit function.
    bool debit(double);
    bool credit(double);
  private:
    double transactionFee;
};


#endif
