/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-04-20
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_SavingsAccount.h"

// Comment 6: Construct parent class Account object in member initializer list
SavingsAccount::SavingsAccount(double initialBalance, double rate)
  : Account(initialBalance), interestRate(rate) {
}

double SavingsAccount::calculateInterest() const {
  return getBalance() * interestRate;
}