/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-02-16
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef DIE_H
#define DIE_H

#include <cstdlib>

class Die {
  public:
    // Comment 1: Constructor contains basic input validation to create dice with at 
    // least one side.
    Die(int numSides); 
    int getSides() const;
    int getValue() const;
    // Comment 2: roll() generate random number and constraint it to between 1 and sides
    void roll(); 
  private:
    int sides;
    int value;
};

#endif