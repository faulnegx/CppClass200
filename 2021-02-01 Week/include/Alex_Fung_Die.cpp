/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-02-16
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_Die.h"

Die::Die(int numSides) : value{0} {
  if (numSides < 1) {
    sides = 1;
  } else {
    sides = numSides;
  }
}

int Die::getSides() const {
  return sides;
}

int Die::getValue() const {
  return value;
}

void Die::roll() {
  value = rand() % sides + 1;
}
