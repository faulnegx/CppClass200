/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-02-16
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_Die.h"
#include <cstdlib>
#include <iostream>

// Comment 3: Start game by inserting splash and seeding random number generator
void splashStartGame() {
  srand(time(0));
  std::cout << "Let's play a game of 21 !\n" << std::endl;
  std::cout << "--------------------------------" << std::endl;
}

// Comment 4: Function to evaluate if user wins.
bool userWin(const int userPoints, const int computerPoints) {
  return userPoints > 21 || (userPoints <=21 && computerPoints <=21 && computerPoints >= userPoints);
}

// Comment 5: Generate printout of the results of the game. 
void endGame(const int userPoints, const int computerPoints) {
  if (userPoints > 0) {
    std::cout << "The computer had " << computerPoints << " points." << std::endl;
    std::cout << "You had " << userPoints << " points.\n" << std::endl;
    if (userWin(userPoints, computerPoints)) {
      std::cout << "Better luck next time." << std::endl;
    } else {
      std::cout << "Congratulations! You won!" << std::endl;
    }
    std::cout << "\n--------------------------------" << std::endl;
  }
  std::cout << "\nGame Over" << std::endl;  
}

// Comment 6: Prompt user and store char in memory.
void userPrompt(char * userInputPtr) {
    std::cout << "Would you like to roll the dice?" << std::endl;
    std::cout << "Enter Y for yes or N for no: ";
    std::cin >> *userInputPtr;
}

int main() {
  splashStartGame();
  // Comment 7: Create two die object both with 6 sides.
  Die userDice(6);
  Die computerDice(6);
  // Comment 8: initialize userPoints, computerPoints and userInput.
  int userPoints = 0;
  int computerPoints = 0;
  char userInput = 'Y';
  // Comment 9: Only continue game if userPoints <=21 and userInput was y(case insensitive)
  while (userPoints <= 21 && (userInput == 'Y' || userInput == 'y')) {
    // Comment 10: Starts round by rolling computer dice and adding it to computerPoints
    computerDice.roll();
    computerPoints += computerDice.getValue();
    userPrompt(&userInput);
    std::cout << std::endl;
    // Comment 11: Only roll user dice if userInput is y(case insensitive)
    if (userInput == 'Y' || userInput == 'y') {
      userDice.roll();
      userPoints += userDice.getValue();
      std::cout << "You have " << userPoints << " points.\n" << std::endl;
    }
  }
  std::cout << "--------------------------------" << std::endl;
  endGame(userPoints, computerPoints);
  return 0;
}
