/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-04-19
Programming Environment Used: Linux, gcc 8.3
*/

#include "RationalNumber.h"

// Comment 1: Term constructors
RationalNumber::RationalNumber(int t, int b): top(t), bottom(b) {
  // Comment 2: use setDenominator as a way of validating input. 
  setDenominator(b);
}

void RationalNumber::printRational() const {
  std::cout << top;
  // Comment 3: Do not deplay the slash and denominator when the bottom is 1. 
  if (bottom != 1) {
    std::cout << "/" << bottom;
  }
}

// Comment 4: simplifyFraction to reduce the fraction to its simpliest form. Calling
// this ensures our fraction always has the simpliest form. 
void RationalNumber::simplifyFraction() {
  // Comment 5: Use STL's built in gcd function to find greatest common divisor.
  int greatestCommonDivisor = std::gcd(top, bottom);
  // Comment 6: Divide both the top and the bottom by the greatest common divisor. 
  top /= greatestCommonDivisor;
  bottom /= greatestCommonDivisor;
}

int RationalNumber::getNumerator() const {
  return top;
}
void RationalNumber::setNumerator(int t) {
  top = t;
  // Comment 7: After changes, we must ensure our fraction has the simpliest form
  simplifyFraction();
}
int RationalNumber::getDenominator() const {
  return bottom;
}
void RationalNumber::setDenominator(int b) {
  // Comment 8: throw exception if we are trying to set the Denominator to 0.
  if (b == 0) {
    throw "Denominator cannot be 0";
  }
  // Comment 9: When trying set bottom as negative, we will set it to positive and flip
  // the sign of the top.
  if (b < 0) {
    b = -1 * b;
    top = -1 * top;
  }
  bottom = b;
  // Comment 10: After changes, we must ensure our fraction has the simpliest form
  simplifyFraction();
}

RationalNumber RationalNumber::operator+(const RationalNumber& other) const {
  int otherTop = other.getNumerator();
  int otherBottom = other.getDenominator();
  int newTop = top * otherBottom + otherTop * bottom;
  int newBottom = bottom * otherBottom;
  return RationalNumber(newTop, newBottom);
}

RationalNumber RationalNumber::operator-(const RationalNumber& other) const {
  return (*this) + -other;
}

RationalNumber RationalNumber::operator-() const {
  return RationalNumber(-1 * top, bottom);
}

RationalNumber RationalNumber::operator*(const RationalNumber& other) const {
  int newTop = top * other.getNumerator();
  int newBottom = bottom * other.getDenominator();
  return RationalNumber(newTop, newBottom);
}

RationalNumber RationalNumber::operator/(const RationalNumber& other) const {
  int newTop = top * other.getDenominator();
  int newBottom = bottom * other.getNumerator();
  return RationalNumber(newTop, newBottom);
}

const RationalNumber& RationalNumber::operator=(const RationalNumber& other) {
  top = other.getNumerator();
  bottom = other.getDenominator();
  return *this;
}

bool RationalNumber::operator>(const RationalNumber& other) const {
  int otherTop = other.getNumerator();
  int otherBottom = other.getDenominator();
  return top * otherBottom > otherTop * bottom;
}

bool RationalNumber::operator==(const RationalNumber& other) const {
  int otherTop = other.getNumerator();
  int otherBottom = other.getDenominator();
  return top == otherTop && bottom == otherBottom;
}
// Comment 11: Reduce re-writing logic by using > and == to define >=, <, !=, <=
bool RationalNumber::operator>=(const RationalNumber& other) const {
  return (*this) > other || (*this) == other;
}

bool RationalNumber::operator<(const RationalNumber& other) const {
  return !((*this) >= other);
}

bool RationalNumber::operator!=(const RationalNumber& other) const {
  return !((*this) == other);
}

bool RationalNumber::operator<=(const RationalNumber& other) const {
  return !((*this) > other);
}
