/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-04-19
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef RATIONALNUMBER_H
#define RATIONALNUMBER_H

#include <iostream>
#include <numeric> // std::gcd

class RationalNumber {
  public:
    RationalNumber(int t = 0, int b = 1);
    void printRational() const;
    int getNumerator() const;
    void setNumerator(int);
    int getDenominator() const;
    void setDenominator(int);
    RationalNumber operator+(const RationalNumber&) const;
    RationalNumber operator-(const RationalNumber&) const;
    RationalNumber operator-() const;
    RationalNumber operator*(const RationalNumber&) const;
    RationalNumber operator/(const RationalNumber&) const;
    const RationalNumber& operator=(const RationalNumber&);
    bool operator>(const RationalNumber&) const;
    bool operator==(const RationalNumber&) const;
    bool operator>=(const RationalNumber&) const;
    bool operator<(const RationalNumber&) const;
    bool operator!=(const RationalNumber&) const;
    bool operator<=(const RationalNumber&) const;
  private:
    void simplifyFraction();
    int top;
    int bottom;
};

#endif
