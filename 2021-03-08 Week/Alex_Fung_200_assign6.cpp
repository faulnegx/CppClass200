/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-03-22
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_DeckOfCards.h"
// Comment 8: Not including Card.h because it is included in DeckOfCards.h already and
// we are not directly making instance of Card.
#include <ctime>
#include <iostream>

int main()
{
  // Comment 9: Application program is responsible to seed the pseudo-random number generator
  std::srand(std::time(0));
  DeckOfCards dc{};
  dc.shuffle();
  // Comment 10: Continue to dealCard until there moreCards returns false.
  while (dc.moreCards()) {
    // Comment 11: call toString method on each dealt Card to output it to stdout
    std::cout << dc.dealCard().toString() << std::endl;
  }
  return 0;
}
