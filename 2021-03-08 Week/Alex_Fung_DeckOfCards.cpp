/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-03-22
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_DeckOfCards.h"

DeckOfCards::DeckOfCards() : deck{}, currentCard{0} {
  // Comment 3: In default constructor, push_back Card one at a time to the end of the deck.
  for (int i = 0; i < 13; ++i) {
    for (int j = 0; j < 4; ++j) {
      deck.push_back(Card(i, j));
    }
  }
}

bool DeckOfCards::moreCards() const {
  // Comment 4: const function does not modify any members. 
  // Returns boolean of currentCard less than 52.
  return currentCard<52;
}

Card DeckOfCards::dealCard() {
  // Comment 5: increment currentCard but return a copy of the currentCard-1 Card in the deck.
  ++currentCard;
  return deck[currentCard-1];
}

void DeckOfCards::shuffle() {
  std::size_t val = 0;
  // Comment 6: Continue to swap cards until we iterated through the entire deck vector
  while (val < deck.size()) {
    // Comment 7: swap each card in the deck with a random card in the deck
    std::swap(deck[val], deck[std::rand() % deck.size()]);
    ++val;
  }
}
