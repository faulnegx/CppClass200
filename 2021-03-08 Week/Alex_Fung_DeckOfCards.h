/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-03-22
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef DECKOFCARDS_H
#define DECKOFCARDS_H

// Comment 1: DeckOfCards needs Card class to work.
#include "Alex_Fung_Card.h"

#include <cstdlib>
#include <string>
#include <vector>

class DeckOfCards {
  public:
    DeckOfCards();
    bool moreCards() const;
    Card dealCard();
    void shuffle();
  private:
    std::vector<Card> deck;
    int currentCard;
};

#endif
