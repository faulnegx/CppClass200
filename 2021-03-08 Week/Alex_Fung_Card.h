/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-03-22
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef CARD_H
#define CARD_H

#include <string>

class Card {
  public:
    Card(int, int);
    std::string toString() const;
  private:
    static std::string facesArray[];
    static std::string suitsArray[];
    int face;
    int suit;
};

#endif
