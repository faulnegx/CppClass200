/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-03-22
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_Card.h"

Card::Card(int f, int s) : face{f}, suit{s}{
}
std::string Card::toString() const {
  return (facesArray[face] + " of " + suitsArray[suit]);
}

// Comment 2: Initialize static variables in cpp implementation file. 
std::string Card::facesArray[]{"Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
std::string Card::suitsArray[]{"Hearts", "Diamonds", "Clubs", "Spades"};
