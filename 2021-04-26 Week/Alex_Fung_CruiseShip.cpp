/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-05-05
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_CruiseShip.h"

// Comment 9: In CruiseShip initializer list, we create the base class object first.
CruiseShip::CruiseShip(std::string n, std::string y, int m)
: Ship(n, y), maxPassengers(m) {
}

int CruiseShip::getMaxPassengers() const {
  return maxPassengers;
}

void CruiseShip::setMaxPassengers(int m) {
  maxPassengers = m;
}

void CruiseShip::print() const {
  // Comment 10: CruiseShip does not have access to name since name is private to
  // Ship. print function now has to use the accessor to get name.
  std::cout << "Name: " << this->getName() << std::endl;
  std::cout << "Max Passengers: " << maxPassengers << std::endl;
}
