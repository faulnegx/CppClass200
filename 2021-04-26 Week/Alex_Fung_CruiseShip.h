/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-05-12
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef CRUISESHIP_H
#define CRUISESHIP_H

#include "Alex_Fung_Ship.h"
// Comment 5: We do not need to include <iostream> or <string> because they are
// already included in Ship.h

// Comment 6: CruiseShip inherits Ship with public inheritance.
class CruiseShip : public Ship {
  public:
    CruiseShip(std::string, std::string, int);
    int getMaxPassengers() const;
    void setMaxPassengers(int);
    // Comment 7: CruiseShip print function is has virtual and override specifier
    // to make clear that this print functions override the base class's print
    // function.
    virtual void print() const override;
    // Comment 8: CruiseShip destructor is automatically marked with virtual
    // since CruiseShip inherits from Ship and Ship's destructor is virtual. We
    // left CruiseShip destructor off.s
  private:
    int maxPassengers;
};

#endif
