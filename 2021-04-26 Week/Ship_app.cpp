#include <iostream>
#include <string>
#include "Alex_Fung_Ship.h"
#include "Alex_Fung_CruiseShip.h"
#include "Alex_Fung_CargoShip.h"
using namespace std;


int main()
{
    // Create an array of Ship pointers, initialized with
    // the addresses of 3 dynamically allocated objects.
    Ship *ships[3] = { new Ship("USS Hornet", "1912"),
                       new CruiseShip("Carnival", "1913", 1500),
                       new CargoShip("Evergreen", "1914", 50000)
                     };

    // Call each object's print function using polymorphism.
    for (int index=0; index < 3; index++)
    {
        ships[index]->print();
        cout << "----------------------------\n";
        
        delete ships[index];     //release memory (avoid memory leak)
        ships[index] = nullptr;   //avoid dangling pointer
    }
    
           
    // system("PAUSE");
    return 0;
}
