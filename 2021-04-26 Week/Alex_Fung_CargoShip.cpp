/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-05-05
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_CargoShip.h"

CargoShip::CargoShip(std::string n, std::string y, int c)
: Ship(n, y), cargoCapacity(c) {
}

int CargoShip::getCargoCapacity() const {
  return cargoCapacity;
}

void CargoShip::setCargoCapacity(int c) {
  cargoCapacity = c;
}

void CargoShip::print() const {
  std::cout << "Name: " << this->getName() << std::endl;
  std::cout << "Cargo Capacity: " << cargoCapacity << std::endl;
}
