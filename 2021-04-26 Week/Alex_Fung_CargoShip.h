/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-05-12
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef CARGOSHIP_H
#define CARGOSHIP_H

#include "Alex_Fung_Ship.h"

class CargoShip : public Ship {
  public:
    CargoShip(std::string, std::string, int);
    int getCargoCapacity() const;
    void setCargoCapacity(int);
    virtual void print() const override;
  private:
    int cargoCapacity;
};

#endif
