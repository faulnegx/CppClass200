/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-05-05
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_Ship.h"

Ship::Ship(std::string n, std::string y): name(n), yearBuilt(y) {
}

std::string Ship::getName() const {
  return name;
}

std::string Ship::getYearBuilt() const {
  return yearBuilt;
}

void Ship::setName(std::string n) {
  name = n;
}

void Ship::setYearBuilt(std::string y) {
  y = yearBuilt;
}

void Ship::print() const {
  std::cout << "Name: " << name << std::endl;
  std::cout << "Year built: " << yearBuilt << std::endl;
}

Ship::~Ship() {
  // Comment 4: No special clean up is required here because Ship does not have
  // any pointer type variables. Strings cleans up itself (as opposed to C-style 
  // strings)
}
