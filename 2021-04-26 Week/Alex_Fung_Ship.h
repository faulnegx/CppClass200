/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-05-12
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef SHIP_H
#define SHIP_H

#include <iostream>
#include <string>

class Ship {
  public:
    // Comment 1: Constructor for Ship requires 2 strings to set name and yearBuilt
    Ship(std::string, std::string);
    std::string getName () const;
    std::string getYearBuilt() const;
    void setName(std::string);
    void setYearBuilt(std::string);
    // Comment 2: print function is declared to be virtual meaning derived class
    // can override print function's behavior
    virtual void print() const;
    // Comment 3: destructor must be declared virtual for derived class to call 
    // base destructors.
    virtual ~Ship();
  private:
    std::string name;
    std::string yearBuilt;
};

#endif
