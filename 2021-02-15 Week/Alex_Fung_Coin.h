/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-03-01
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef COIN_H
#define COIN_H

#include <cstdlib>
#include <string>

// Comment 1: assumes srand is called by application program
class Coin {
  public:
    Coin (); 
    void toss();
    std::string getSideUp();
  private:
    std::string sideUp;
};

#endif