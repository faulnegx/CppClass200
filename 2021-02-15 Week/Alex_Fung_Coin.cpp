/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-03-01
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_Coin.h"

Coin::Coin() : sideUp{""} {
  // Comment 2: Calls toss to randomly determine sideUp at coin construction
  toss();
}

void Coin::toss() {
  if (rand() % 2) {
    sideUp = "heads";
  } else {
    sideUp = "tails";
  }
}

std::string Coin::getSideUp() {
  return sideUp;
}
