/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-03-01
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_Coin.h"
#include <iostream>

// Comment 3: Start game by seeding random number generator and printing initial balance
void splashStartGame() {
  srand(time(0));
  std::cout << "Your starting balance: $0.00" << std::endl;
  std::cout << "The computer's starting balance: $0.00\n" << std::endl;
}

// Comment 4: returns True if coin's sideUp equal heads
bool isHeads(Coin& c) {
  return (c.getSideUp() == "heads");
}

void tossThreeCoin(Coin& coin1, Coin& coin2, Coin& coin3) {
  coin1.toss();
  coin2.toss();
  coin3.toss();
}

// Comment 5: pdate Balance based on if coin has "heads"
void updateBalance(float& balance, Coin& nickel, Coin& dime, Coin& quarter) {
  tossThreeCoin(nickel, dime, quarter);
  if (isHeads(nickel)) {
    balance += 0.05;
  } 
  if (isHeads(dime)) {
    balance += 0.1;
  } 
  if (isHeads(quarter)) {
    balance += 0.25;
  } 
}

// Comment 6: Function to print out ending balances and determine winner.
void endGame(const float playerBalance, const float computerBalance) {
  std::cout << "Your ending balance: $" << playerBalance << std::endl;
  std::cout << "The computer's ending balance: $" << computerBalance << std::endl;
  std::cout << std::endl;

  if (playerBalance > computerBalance) {
    std::cout << "Congratulations! You won." << std::endl;
  } else if (computerBalance > playerBalance) {
    std::cout << "Sorry! The computer won." << std::endl;
  } else {
    std::cout << "Tie! Nobody wins." << std::endl;
  }
  std::cout << std::endl;
}

int main() {
  splashStartGame();
  // Comment 7: Set float to print 2 decimal places always. 
  std::cout.precision(2);
  std::cout << std::fixed;
  // Comment 8: Make 3 coins.
  Coin dime{};
  Coin nickel{};
  Coin quarter{};
  // Comment 9: Initialize balance for both computer and player. 
  float playerBalance{0};
  float computerBalance{0};
  int round = 0;
  // Comment 10: toss all coins and add to Balance until one of the balance >= 1
  while (playerBalance < 1 && computerBalance < 1) {
    ++round;
    updateBalance(playerBalance, nickel, dime, quarter);
    updateBalance(computerBalance, nickel, dime, quarter);
    std::cout << "Your balance after round " << round << ": $";
    std::cout << playerBalance << std::endl;
    std::cout << "The computer's balance after round " << round << ": $";
    std::cout << computerBalance << std::endl;
    std::cout << std::endl;
  }
  endGame(playerBalance, computerBalance);
  return 0;
}
