/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-03-07
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_FuelGauge.h"

FuelGauge::FuelGauge(int initialFuel) : fuel{15} {
  // Comment 1: Only allow initialFuel value to be within 0 and 15
  if (initialFuel >= 0  && initialFuel <= 15) {
    fuel = initialFuel;
  } else {
    std::cout << "initialFuel out of range(0,15). fuel set to 15." << std::endl;
  }
}

void FuelGauge::incrementFuel() {
  // Comment 2: Refuse to incrementFuel when fuel is at maxFuel.
  if (fuel < maxFuel) {
    ++fuel;
  } else {
    std::cout << "Fuel already at max. Fuel not incremented." << std::endl;
  }
}

void FuelGauge::decrementFuel() {
  // Comment 3: Refuse to decrementFuel when fuel is already at 0.
  if (fuel > 0) {
    --fuel;
  } else {
    std::cout << "Fuel is less than or equal to 0. Fuel not decremented." << std::endl;
  }
}

int FuelGauge::getFuel() const {
  return fuel;
}
