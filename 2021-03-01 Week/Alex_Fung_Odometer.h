/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-03-07
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef ODOMETER_H
#define ODOMETER_H

#include "Alex_Fung_FuelGauge.h"
#include <iostream>

class Odometer {
  public:
    Odometer (int, FuelGauge&); 

    void incrementMileage();
  
    int getMileage() const;

  private:
    int mileage;
    FuelGauge& fuelGauge;
    int gallonMilesCounter{0};
    const int milesPerGallon{24};
    const int maxMileage{999999};
};

#endif