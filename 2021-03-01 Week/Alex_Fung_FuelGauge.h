/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-03-07
Programming Environment Used: Linux, gcc 8.3
*/

#ifndef FUELGAUGE_H
#define FUELGAUGE_H

#include <iostream>

class FuelGauge {
  public:
    FuelGauge (int);

    void incrementFuel();
    void decrementFuel();
  
    int getFuel() const;

  private:
    int fuel;
    const int maxFuel{15};
};

#endif