/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-03-07
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_FuelGauge.h"
#include "Alex_Fung_Odometer.h"
#include <iostream>

int main() {
  // Comment 7: Initialize FuelGauge with inital fuel of 0
  FuelGauge fuelGauge{0};
  // Comment 8: incrementFule until fuel is 15 times to fill up the fuelGauge.
  std::cout << "Fuel level: " << fuelGauge.getFuel() << " gallons." << std::endl;
  while(fuelGauge.getFuel() < 15) {
    fuelGauge.incrementFuel();
    std::cout << "Fuel level: " << fuelGauge.getFuel() << " gallons." << std::endl;
  }
  // Comment 9: Initialize odometer with 0 miles and fuelGauge.
  Odometer odo{0, fuelGauge};
  // Comment 10: IncrementMileage until fuelgauge hit 0.
  while(fuelGauge.getFuel() > 0) {
    odo.incrementMileage();
    std::cout << "\nMileage: " << odo.getMileage() << std::endl;
    std::cout << "Fuel level: " << fuelGauge.getFuel() << " gallons." << std::endl;
    std::cout << "-------------------------------------" << std::endl;
  }
  return 0;
}
