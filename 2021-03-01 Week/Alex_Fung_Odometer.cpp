/*
Programmer: Alex Fung
Programmer's ID: 1873719
Class: COMPSC-200-2322
Date: 2021-03-07
Programming Environment Used: Linux, gcc 8.3
*/

#include "Alex_Fung_Odometer.h"

Odometer::Odometer(int initialMileage, FuelGauge& fg) : mileage{0}, fuelGauge{fg} {
  // Comment 4: Only allow initialMileage value to be within 0 and 999999
  if (initialMileage >= 0  && initialMileage <= 999999) {
    mileage = initialMileage;
  } else {
    std::cout << "initialMileage out of range(0,999999). mileage set to 0." << std::endl;
  }
}

void Odometer::incrementMileage() {
  // Comment 5: Only incrementMileage if fuelGauge has more than 0 fuel
  if (fuelGauge.getFuel() > 0) {
    ++mileage;
    ++gallonMilesCounter;
    if (mileage > maxMileage) {
      mileage = 0;
    }
    // Comment 6: Decrement Fuel after every milesPerGallon
    if (gallonMilesCounter % milesPerGallon == 0) {
      gallonMilesCounter = 0;
      fuelGauge.decrementFuel();
    }
  } else {
    std::cout << "fuelGauge empty. Please increment fuel first." << std::endl;
  }
}

int Odometer::getMileage() const {
  return mileage;
}