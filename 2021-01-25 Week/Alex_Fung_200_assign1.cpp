#include <iostream>
#include <limits> // Comment 1: The program will be using std::numeric_limits in this header
using namespace std;

const int COLS = 5; // Comment 2: Global constant COL is defined. 

// Comment 3: Here we are declaring all the functions. Definition will come later
int getTotal(int [][COLS], int);
double getAverage(int [][COLS], int);
int getRowTotal(int [][COLS], int);
int getColumnTotal(int [][COLS], int, int);
int getHighestInRow(int [][COLS], int);
int getLowestInRow(int [][COLS], int);
// Comment 4: I added a helper function to run all the test runs in one go.
void runTestRun(int [][COLS], int, int, int);

int main()
{
	int testArray0[6][COLS] =
					{ { 53,  20, 62, 22, 60 },
					{   68,  48, 45, 72, 88 },
					{   70,  99, 43, 49, 46 },
					{   88,  33, 24, 74, 51 },
					{   76,  98, 97, 13, 10 },
					{   84,  60, 88, 84,  5 } };
	// Comment 5: This is testrun 0 and testArray0 is a 6x5 array. W
	// We are interested in row 0 and col 0. 
	runTestRun(testArray0, 6, 0, 0);

	int testArray1[5][COLS] =
					{ { 53,  20, 62, 22, 60 },
					{   43,  59, 10,  7, 14 },
					{   78,  43, 87, 35, 94 },
					{    3,  66,  5, 94, 94 },
					{   98,  63, 86, 80, 63 } };
	// Comment 6: This is testrun 1 and testArray1 is a 5x5 array. W
	// We are interested in row 0 and col 1. The rest of the function calls are similar.
	runTestRun(testArray1, 5, 0, 1);

	int testArray2[2][COLS] =
					{ { 66,   8, 43, 16, 38 },
					{   35,  95,  9, 20, 29 } };
	runTestRun(testArray2, 2, 1, 2);

	int testArray3[3][COLS] =
					{ { 35,  91, 87, 57, 63 },
					{   74,  81, 49,  2, 74 },
					{   56,  87, 35, 81, 65 } };
	runTestRun(testArray3, 3, 2, 3);

	int testArray4[4][COLS] =
					{ { 89,  75, 76, 87, 33 },
					{   14,  22, 86, 89, 40 },
					{   14,  92, 50, 82, 29 },
					{   40,  38, 34, 43, 10 } };
	runTestRun(testArray4, 4, 3, 4);        
	return 0;
}


int getTotal(int inArray[][COLS], int rows)
{
	// Comment 7: We are iterating through all the rows and within each row, all through
	// the columns. Adding element to sum and returning sum.
	int sum = 0;
	for (int i =0; i < rows; ++i) {
		for (int j = 0; j < COLS; ++j) {
			sum += inArray[i][j];
		}
	}
	return sum;
}


double getAverage(int inArray[][COLS], int rows)
{
	// Comment 8: We are iterating through all the rows and within each row, all through
	// the columns. Adding element to sum and returning sum. the ints are implicitly 
	// converted to double. At the very end, we divide the sum by number of elements to 
	// get the average and return that value. 
	double sum = 0.0;
	for (int i =0; i < rows; ++i) {
		for (int j = 0; j < COLS; ++j) {
			sum += inArray[i][j];
		}
	}
	return sum/(COLS*rows);

}


int getRowTotal(int inArray[][COLS], int rowToTotal)
{
	int sum = 0;
	for (int i = 0;i < COLS; ++i) {
		sum += inArray[rowToTotal][i];
	}
	return sum;
}


int getColumnTotal(int inArray[][COLS], int colToTotal, int rows)
{
	int sum = 0;
	for (int i = 0;i < rows; ++i) {
		sum += inArray[i][colToTotal];
	}
	return sum;
}


int getHighestInRow(int inArray[][COLS], int rowToSearch)
{
	// Comment 9: First set the highest value to min so any other number will be larger. 
	// Then, iterate through the columns in that row. Replace variable highest with any 
	// larger number in the array. After checking through the element in the row, the 
	// largest value will be stored in variable highest. Return that value. 
	int highest = numeric_limits<int>::min();
	for (int i = 0;i < COLS; ++i) {
		if (inArray[rowToSearch][i] > highest) {
			highest = inArray[rowToSearch][i];
		}
	}
	return highest;
}


int getLowestInRow(int inArray[][COLS], int rowToSearch)
{
	// Comment 10: This function is essentially the copy of the getHighestInRow function. 
	// The exception are we start from the largest int possible and we are checking for 
	// any smaller number in the array. 
	int lowest = numeric_limits<int>::max();
	for (int i = 0;i < COLS; ++i) {
		if (inArray[rowToSearch][i] < lowest) {
			lowest = inArray[rowToSearch][i];
		}
	}
	return lowest;
}


void runTestRun(int inArray[][COLS], int rows, int rowOfInterest, int run)
{
	// Comment 11: This helper function to run all the function calls in one go.
	cout << endl;
	cout << "Sample Run " << run << ": " << endl;
	cout << "The total of the array elements is "
		 << getTotal(inArray, rows)
		 << endl;
	cout << "The average value of an element is "
		 << getAverage(inArray, rows)
		 << endl;
	cout << "The total of row " << rowOfInterest << " is "
		 << getRowTotal(inArray, rowOfInterest)
		 << endl;
	// Comment 12: We cheated by utilizing the run's value to be column in 
	// getColumnTotal function call. I hand picked run so it would always be below COLS
	// Otherwise, we could have undefined behavior as we are accessing item out side of
	// the array. 
	cout << "The total of col " << run << " is "
		 << getColumnTotal(inArray, run, rows)
		 << endl;  
	cout << "The highest value in row " << rowOfInterest << " is "
		 << getHighestInRow(inArray, rowOfInterest)
		 << endl;
	cout << "The lowest value in row " << rowOfInterest << " is "
		 << getLowestInRow(inArray, rowOfInterest)
		 << endl;
}